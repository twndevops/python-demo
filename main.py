# To run program, `python3 main.py`.

# import helper
# from helper import *
from helper import user_prompt, days_to_units

while True:
    days_and_unit = input(user_prompt)

    du_list = days_and_unit.split(":")

    if len(du_list) == 1:
        # present starting instruction again
        continue

    du_dict = {
        "days": du_list[0],
        "unit": du_list[1]
    }

    # b/c this is a loop, reset base conversion value
    # conversion = 1

    if du_dict["unit"] == "hrs":
        conversion=24
    elif du_dict["unit"] == "mins":
        conversion=24*60
    elif du_dict["unit"] == "secs":
        conversion=24*60*60
    else:
        # if unit is empty or invalid str
        print("Pass unit 'hrs', 'mins', or 'secs'.")
        # continue loop w/o calling days_to_units func
        continue

    # if importing entire helper module:
    # print(helper.days_to_units(du_dict["days"], conversion, du_dict["unit"]))

    print(days_to_units(du_dict["days"], conversion, du_dict["unit"]))