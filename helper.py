from logging import error

def days_to_units(num_days, convert_to_units, unit):
    try:
        # handle neg whole nums
        if int(num_days)<0:
            return "Enter whole pos num."
        # calculate w/ 0 and pos whole nums
        else:
            return f"{int(num_days)} days are {int(num_days) * convert_to_units} {unit}"
    except ValueError:
        error("Str or decimal input. Enter whole pos num.")

user_prompt = "Enter number of days and unit to convert to, colon-sep.\n"