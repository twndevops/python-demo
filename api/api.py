import requests
# GL API reference for all of a user's projects: https://docs.gitlab.com/ee/api/projects.html#list-user-projects
unatarajan_projects = requests.get("https://gitlab.com/api/v4/users/unatarajan/projects")

# requests pkg reference: https://requests.readthedocs.io/en/latest/user/quickstart/#response-content
print(unatarajan_projects.text) # Does not get any projects, likely b/c they're all in a group (?)

# GL API reference for all of a group's projects: https://docs.gitlab.com/ee/api/groups.html#list-a-groups-projects
twndevops_projects = requests.get("https://gitlab.com/api/v4/groups/77839622/projects") # Auth headers not needed b/c group is public (?)

# results are paginated, each page capped at 20 results
projects_list = twndevops_projects.json()
# print(type(projects_list))
# print(projects_list[0])

for proj in projects_list:
    print(f"{proj.get("name")} project can be found here: {proj["web_url"]}.\n")
