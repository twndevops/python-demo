months_list = ["Jan", "Feb", "Mar"]

# access item in list
print(months_list[1])

# add item to list
months_list.append("Apr")
print(months_list)

# change added item
months_list[len(months_list)-1] = "May"
print(months_list)

# pop off added item
months_list.pop()
print(months_list)

# add dup "Feb"
months_list.append("Feb")
# remove 1st "Feb"
months_list.remove("Feb")
print(months_list)
# remove 2nd "Feb"
months_list.remove("Feb")
print(months_list)

# following throws IndexError
print(months_list[len(months_list)])