months_set = {"Jan", "Feb", "Mar"}

# access items ONLY via loop
for month in months_set:
    print(month)

# add item
months_set.add("Apr")
print(months_set)

# remove item
months_set.remove("Mar")
print(months_set)

# trying to change set vals fails
for month in months_set:
    month = "Sep"
    print(month) # always prints "Sep"
    print(months_set) # unchanged
