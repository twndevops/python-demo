class User:
    def __init__(self, email, name, pwd, job):
        self.email = email
        self.name = name
        self.pwd = pwd
        self.job = job

    def change_pwd(self, new_pwd):
        self.pwd = new_pwd

    def change_job(self, new_job):
        self.job = new_job

    def get_user_info(self):
        print(f"Email {self.name}, {self.job} at {self.email}.")
