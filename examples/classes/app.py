# import user
from user import User
from post import Post

jenkins = User("jenkins@example.com", "Jenkins", "adminpwd", "automation server")
jenkins.get_user_info()
jenkins.change_pwd("jenkinspwd")
# print(jenkins.pwd)
jenkins.change_job("automation agent")
jenkins.get_user_info()

docker = User("docker@io.com", "Docker", "rootpwd", "image registry")
docker.get_user_info()

jenkinsPost = Post("Committed version bump ci", jenkins.name)
jenkinsPost.get_post_info()

dockerPost = Post("New image tag uploaded", docker.name)
dockerPost.get_post_info()
