# import datetime class (/ module def) from Lib datetime (/ module)
from datetime import datetime

milestone = input("Enter your goal and deadline, colon-sep.\n")
milestone_list = milestone.split(":")
goal = milestone_list[0]
deadline = milestone_list[1]

# call strptime func from datetime class
# "24.04.2024" -> 2024-04-24 00:00:00
# only concerned w/ day, not hrs, mins, secs, millisecs
deadline_date = datetime.strptime(deadline, "%d.%m.%Y")
# calculate days from now to deadline
today_date = datetime.today()
time_remaining = deadline_date-today_date

# tell user # days until deadline
# access days prop on timedelta obj
print(f"Time left to {goal}: {time_remaining.days} days!")

# no hours prop on timedelta obj, use total_seconds prop
# convert float to int to lose precise decimal places
time_remaining_hours = int(time_remaining.total_seconds()/3600)
print(f"Time left to {goal}: {time_remaining_hours} hours!")
