import openpyxl

inv_file = openpyxl.load_workbook("inventory/inventory.xlsx")
inv_sheet = inv_file["Sheet1"]

for row in range(2, inv_sheet.max_row+1):
    # for curr row's product, multiply inventory * price
    product_inventory_value = inv_sheet.cell(row, 2).value * inv_sheet.cell(row, 3).value
    # store cell to write to col 5 cell
    product_inventory_value_cell = inv_sheet.cell(row, 5)
    # write curr row's inventory value to cell
    product_inventory_value_cell.value = product_inventory_value

# save writes above to same file
# inv_file.save("inventory/inventory.xlsx")

# save writes to new file based off inventory.xlsx incl writes above
inv_file.save("inventory/inventory-with-product-total-value.xlsx")
