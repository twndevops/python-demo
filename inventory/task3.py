import openpyxl

inv_file = openpyxl.load_workbook("inventory/inventory.xlsx")
inv_sheet = inv_file["Sheet1"]

company_inventory_value = {}

for row in range(2, inv_sheet.max_row+1):
    # store curr row's supplier
    supplier = inv_sheet.cell(row, 4).value
    # store product of curr row's inventory count (col 2) and price per item (col 3)
    product_inventory_value = inv_sheet.cell(row, 2).value * inv_sheet.cell(row, 3).value
    # if supplier exists in dict
    if supplier in company_inventory_value:
        # add curr row's inventory value to existing inventory value for supplier
        # company_inventory_value[supplier] += product_inventory_value
        company_inventory_value[supplier] = company_inventory_value.get(supplier) + product_inventory_value
    else:
        # add supplier to dict w/ curr row's inventory value
        company_inventory_value[supplier] = product_inventory_value

print(company_inventory_value)
