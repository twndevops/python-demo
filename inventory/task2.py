import openpyxl

inv_file = openpyxl.load_workbook("inventory/inventory.xlsx")
inv_sheet = inv_file["Sheet1"]

ten_or_fewer_products = {}

for row in range(2, inv_sheet.max_row+1):
    # store curr row's product inventory count
    product_count = inv_sheet.cell(row, 2).value
    # if fewer than 10 of product are in inventory
    if product_count < 10:
        # store curr row's product id
        product_id = inv_sheet.cell(row, 1).value
        # create new key:val pair in dict
        ten_or_fewer_products[int(product_id)] = int(product_count)

print(ten_or_fewer_products)
