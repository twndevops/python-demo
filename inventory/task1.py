import openpyxl

inv_file = openpyxl.load_workbook("inventory/inventory.xlsx")
inv_sheet = inv_file["Sheet1"]

products_per_supplier = {}

# iterates from row 2 (incl) to row 76 (excl)
for row in range(2, inv_sheet.max_row+1):
    # w/o .value property, storing reference to unique cell in sheet
    supplier = inv_sheet.cell(row, 4).value
    # if supplier exists as key in dict, increment value by 1
    if supplier in products_per_supplier:
        # can access existing supplier key w/ brackets or .get method
        # products_per_supplier[supplier] += 1
        products_per_supplier[supplier] = products_per_supplier.get(supplier) + 1
    # add new key for supplier not yet in dict
    else:
        products_per_supplier[supplier] = 1

print(products_per_supplier)
